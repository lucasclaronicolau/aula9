const express = require('express');
const app = express();
const dados = require('../data/index.js')
const router = express.Router()

app.use(express.json())


router.get('/' , (req, res) =>{
	res.json(dados.usuarios);
})

router.get('/:id' , (req, res) =>{
	res.json(dados.usuarios.find( record => record._id == req.params.id));
})

router.post('/', (req, res) =>{
	var data = req.body;
	dados.usuarios.push(data);
	res.json(data);
})

router.put('/:id', (req, res) =>{
	var data = req.body;
	var id = req.params.id;
	var anterior = dados.usuarios.find( record => record._id == req.params.id);	
	for(i= 0 ; i < dados.usuarios.length; i++){
        if(dados.usuarios[i]._id == id){
			dados.usuarios[i] = data
		}
    };
	res.json(anterior);
})

router.delete('/:id', (req, res) =>{
	var data = req.body;
	var id = req.params.id;
	var anterior = dados.usuarios.find( record => record._id == req.params.id);	
	for(i= 0 ; i < dados.usuarios.length; i++){
        if(dados.usuarios[i]._id == id){
			dados.usuarios.splice(i,1);
		}
    };
	res.json(anterior);
})

module.exports = router;