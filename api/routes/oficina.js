const express = require('express');
const app = express();
const dados = require('../data/index.js')
const router = express.Router()

app.use(express.json())


router.get('/' , (req, res) =>{
	res.json(dados.oficina);
})

router.get('/:id' , (req, res) =>{
	res.json(dados.oficina.find( record => record._id == req.params.id));
})

router.post('/', (req, res) =>{
	var data = req.body;
	dados.oficina.push(data);
	res.json(data);
})

router.put('/:id', (req, res) =>{
	var data = req.body;
	var id = req.params.id;
	var anterior = dados.oficina.find( record => record._id == req.params.id);	
	for(i= 0 ; i < dados.oficina.length; i++){
        if(dados.oficina[i]._id == id){
			dados.oficina[i] = data
		}
    };
	res.json(anterior);
})

router.delete('/:id', (req, res) =>{
	var data = req.body;
	var id = req.params.id;
	var anterior = dados.oficina.find( record => record._id == req.params.id);	
	for(i= 0 ; i < dados.oficina.length; i++){
        if(dados.oficina[i]._id == id){
			dados.oficina.splice(i,1);
		}
    };
	res.json(anterior);
})

module.exports = router;