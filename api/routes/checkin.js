const express = require('express');
const app = express();
const dados = require('../data/index.js')
const router = express.Router()

app.use(express.json())


router.post('/:oficina/:id' , (req, res) =>{
	var oficina = req.params.oficina;
	for(i= 0 ; i < dados.oficina.length; i++){	
		if(dados.oficina[i]._id == oficina){
			if(!dados.usuarios.find( record => record._id == req.params.id) == ""){
				if(dados.oficina[i].participantes.length ==  dados.local.find( record => record._id == dados.oficina[i].local).capacidade){
					return res.json({"erro": {"msg": "A capacidade máxima foi atingida", "codigo": 400}});				
				}else{
					var user = Number.parseInt(req.params.id);
					if(dados.oficina[i].participantes.includes(user)){
						return res.json({"erro": {"msg": "Usuário Já Logado","codigo": 200}});
					}else{						
						dados.oficina[i].participantes.push(user)
						return res.json(dados.usuarios.find( record => record._id == user))
					}
				}
			}else{
				return res.json({"erro": {"msg": "Usuário Não Encontrada","codigo": 404}});
			}
		}else{
			return res.json({"erro": {"msg": "Oficina Não Encontrada","codigo": 404}});
		}
	}
});


router.delete('/:oficina/:id' , (req, res) =>{
	var user = Number.parseInt(req.params.id);
	var oficina = req.params.oficina;
	for(i= 0 ; i < dados.oficina.length; i++){
		if(dados.oficina[i]._id == oficina){
			if(dados.oficina[i].participantes.includes(user)){
				var usuarioRemovido = dados.usuarios.find( record => record._id == user)
				dados.oficina[i].participantes.splice(dados.oficina[i].participantes.indexOf(user),1);
				return res.json(usuarioRemovido);
			}else{
				return res.json({"erro": {"msg": "Usuário Não Está Logado","codigo": 401}});
			}
		}else{
			return res.json({"erro": {"msg": "Oficina Não Encontrada","codigo": 404}});
		}
	}
});

module.exports = router;