const express = require('express');
const oficina = require('./api/routes/oficina.js');
const local = require('./api/routes/local.js');
const usuarios = require('./api/routes/usuarios.js');
const checkin = require('./api/routes/checkin.js');
const app = express();

app.use(express.json())
app.use('/oficina', oficina);
app.use('/local', local);
app.use('/usuarios', usuarios);
app.use('/checkin', checkin);

app.listen(80, () =>{
	console.log('Rodando');	
})